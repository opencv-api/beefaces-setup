***The Dockerfile in this repo was built from instructions**
Source: https://github.com/justadudewhohacks/opencv4nodejs-docker-images/tree/master/opencv-nodejs

###Steps

Clone or Download this repo on your local machine

Within the folder, add first execution permission to the build.sh script then execute by taping:

./build.sh <OpenCV version> <with contrib?> <node major version>

Build OpenCV 3.3.1 with contrib and node version 9.x:

./build.sh 3.3.1 y 9

By executing the above command in terminal, you will end up with a docker image "amydoxym/opencv-nodejs:node9-opencv3.3.1-contrib" 
